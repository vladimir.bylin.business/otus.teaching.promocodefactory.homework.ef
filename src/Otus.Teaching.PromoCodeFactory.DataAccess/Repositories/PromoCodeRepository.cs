﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class PromoCodeRepository : EfAbstractRepository<PromoCode>, IPromoCodeRepository
{
    public PromoCodeRepository(DatabaseContext dbContext) : base(dbContext)
    {
    }

    public async Task<List<PromoCode>> GetAllAsync()
    {
        return await GetAll().ToListAsync();
    }

    public async Task<PromoCode> GetByIdAsync(Guid id)
    {
        return await GetByCondition(c => c.Id == id)
            .FirstOrDefaultAsync();
    }

    public async Task<Guid> CreateAsync(PromoCode promoCode)
    {
        promoCode.Id = new Guid();
        Create(promoCode);
        
        await SaveChangesAsync();

        return promoCode.Id;
    }

    public async Task UpdateAsync(PromoCode promoCode)
    {
        Update(promoCode);

        await SaveChangesAsync();
    }

    public async Task UpdateRangeAsync(IEnumerable<PromoCode> promoCodes)
    {
        await EntitySet.AddRangeAsync(promoCodes);

        await SaveChangesAsync();
    }

    public async Task DeleteAsync(Guid id)
    {
        var promoCodeToDelete = await GetByIdAsync(id);
        Delete(promoCodeToDelete);

        await SaveChangesAsync();
    }
}