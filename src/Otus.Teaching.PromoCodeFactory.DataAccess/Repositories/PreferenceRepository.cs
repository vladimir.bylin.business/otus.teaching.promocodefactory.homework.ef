﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class PreferenceRepository : EfAbstractRepository<Preference>, IPreferenceRepository
{
    public PreferenceRepository(DatabaseContext dbContext) : base(dbContext)
    {
    }

    public async Task<List<Preference>> GetAllAsync()
    {
        return await GetAll().ToListAsync();
    }

    public async Task<Preference> GetByIdAsync(Guid id)
    {
        return await GetByCondition(c => c.Id == id)
            .FirstOrDefaultAsync();
    }

    public async Task<Guid> CreateAsync(Preference preference)
    {
        preference.Id = new Guid();
        Create(preference);
        
        await SaveChangesAsync();

        return preference.Id;
    }

    public async Task UpdateAsync(Preference preference)
    {
        Update(preference);

        await SaveChangesAsync();
    }

    public async Task DeleteAsync(Guid id)
    {
        var preferenceToDelete = await GetByIdAsync(id);
        Delete(preferenceToDelete);

        await SaveChangesAsync();
    }
}