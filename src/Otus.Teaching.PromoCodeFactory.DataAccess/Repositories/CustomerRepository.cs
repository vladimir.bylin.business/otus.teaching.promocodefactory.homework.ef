﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class CustomerRepository : EfAbstractRepository<Customer>, ICustomerRepository
{
    public CustomerRepository(DatabaseContext dbContext) : base(dbContext)
    {
    }

    public async Task<List<Customer>> GetAllAsync()
    {
        return await GetAll().ToListAsync();
    }

    public async Task<Customer> GetByIdAsync(Guid id)
    {
        return await GetByCondition(c => c.Id == id)
            .Include(c => c.Preferences)
            .Include(c => c.PromoCodes)
            .FirstOrDefaultAsync();
    }

    public async Task<Customer> GetByIdForUpdateAsync(Guid id)
    {
        return await EntitySet.Where(c => c.Id == id)
            .Include(c => c.Preferences)
            .Include(c => c.PromoCodes)
            .Include(c => c.CustomerPreferences)
            .Include(c => c.CustomerPromoCodes)
            .FirstOrDefaultAsync();
    }

    public async Task<List<Customer>> GetAllWithGivenPreference(Guid preferenceId)
    {
        return await GetByCondition(c => c.Preferences.Any(p => p.Id == preferenceId))
            .ToListAsync();
    }

    public async Task<Guid> CreateAsync(Customer customer)
    {
        customer.Id = new Guid();
        Create(customer);
        
        await SaveChangesAsync();

        return customer.Id;
    }

    public async Task<Guid> CreateWithPreferencesAsync(Customer customer)
    {
        customer.Id = new Guid();
        Create(customer);
        Context.Set<Preference>().AttachRange(customer.Preferences);
        
        await SaveChangesAsync();

        return customer.Id;
    }
    
    public async Task UpdateAsync(Customer customer)
    {
        Update(customer);

        await SaveChangesAsync();
    }

    public async Task DeleteAsync(Guid id)
    {
        var customerToDelete = await GetByIdAsync(id);
        Delete(customerToDelete);
        
        // Удалить привязки предпочтений 
        var customerPreferences = Context.Set<CustomerPreference>()
            .Where(cp => cp.CustomerId == id);
        Context.Set<CustomerPreference>().RemoveRange(customerPreferences);
        
        // Удалит привязки промокодов
        var customerPromoCodes = Context.Set<CustomerPromoCode>()
            .Where(cp => cp.CustomerId == id);
        Context.Set<CustomerPromoCode>().RemoveRange(customerPromoCodes);

        await SaveChangesAsync();
    }
}