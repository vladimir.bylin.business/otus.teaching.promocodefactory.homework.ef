﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public abstract class EfAbstractRepository<TEntity> : IBaseRepository<TEntity> where TEntity: BaseEntity
{
    protected readonly DatabaseContext Context;
    protected readonly DbSet<TEntity> EntitySet;

    protected EfAbstractRepository(DatabaseContext dbContext)
    {
        Context = dbContext;
        EntitySet = Context.Set<TEntity>();
    }
        
    public IQueryable<TEntity> GetAll() => 
        EntitySet.AsNoTracking();

    public IQueryable<TEntity> GetByCondition(Expression<Func<TEntity, bool>> expression) => 
        EntitySet.Where(expression).AsNoTracking();

    public void Create(TEntity entity) =>
        EntitySet.Add(entity);

    public void Update(TEntity entity) =>
        EntitySet.Update(entity);

    public void Delete(TEntity entity) =>
        EntitySet.Remove(entity);

    public async Task SaveChangesAsync()
    {
        await Context.SaveChangesAsync();
    }
}