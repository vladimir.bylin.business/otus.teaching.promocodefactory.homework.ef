﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

/// <summary>
/// Контекст бд
/// </summary>
public sealed class DatabaseContext : DbContext
{
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
    {
    }

    public DbSet<Employee> Employees { get; set; }
    
    public DbSet<Role> Roles { get; set; }
    
    public DbSet<Customer> Customers { get; set; }
    
    public DbSet<Preference> Preferences { get; set; }
    
    public DbSet<PromoCode> PromoCodes { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
        optionsBuilder.EnableSensitiveDataLogging();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        
        modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
        modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
        
        modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
        modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);
    }
}