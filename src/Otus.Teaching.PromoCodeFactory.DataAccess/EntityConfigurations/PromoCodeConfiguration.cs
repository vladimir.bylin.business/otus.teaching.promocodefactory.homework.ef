﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations;

public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
{
    public void Configure(EntityTypeBuilder<PromoCode> builder)
    {
        builder.HasKey(p => p.Id);

        builder
            .Property(p => p.Code)
            .HasMaxLength(50);
        
        builder
            .Property(p => p.ServiceInfo)
            .HasMaxLength(100);
        
        builder
            .Property(p => p.PartnerName)
            .HasMaxLength(50);

        builder
            .HasOne(p => p.PartnerManager)
            .WithMany(e => e.PromoCodes)
            .HasForeignKey(p => p.PartnerManagerId);
        
        builder
            .HasOne(p => p.Preference)
            .WithMany(e => e.PromoCodes)
            .HasForeignKey(p => p.PreferenceId);
    }
}