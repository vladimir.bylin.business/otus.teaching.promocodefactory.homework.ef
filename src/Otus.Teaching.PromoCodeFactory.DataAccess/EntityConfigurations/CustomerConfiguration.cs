﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations;

public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
{
    public void Configure(EntityTypeBuilder<Customer> builder)
    {
        builder.HasKey(c => c.Id);
        
        builder
            .Property(c => c.FirstName)
            .HasMaxLength(20);
        
        builder
            .Property(c => c.LastName)
            .HasMaxLength(20);

        builder
            .Property(c => c.Email)
            .HasMaxLength(100);
        
        builder
            .HasMany(c => c.Preferences)
            .WithMany(p => p.Customers)
            .UsingEntity<CustomerPreference>(
                r => r
                    .HasOne(cp => cp.Preference)
                    .WithMany(p => p.CustomerPreferences)
                    .HasForeignKey(cp => cp.PreferenceId),
                l => l
                    .HasOne(cp => cp.Customer)
                    .WithMany(c => c.CustomerPreferences)
                    .HasForeignKey(cp => cp.CustomerId),
                b => b
                    .HasKey(cp => new { cp.CustomerId, cp.PreferenceId })
            );
        
        builder
            .HasMany(c => c.PromoCodes)
            .WithMany(p => p.Customers)
            .UsingEntity<CustomerPromoCode>(
                r => r
                    .HasOne(cp => cp.PromoCode)
                    .WithMany(p => p.CustomerPromoCodes)
                    .HasForeignKey(cp => cp.PromoCodeId),
                l => l
                    .HasOne(cp => cp.Customer)
                    .WithMany(c => c.CustomerPromoCodes)
                    .HasForeignKey(cp => cp.CustomerId),
                b => b
                    .HasKey(cp => new { cp.CustomerId, cp.PromoCodeId })
            );
    }
}