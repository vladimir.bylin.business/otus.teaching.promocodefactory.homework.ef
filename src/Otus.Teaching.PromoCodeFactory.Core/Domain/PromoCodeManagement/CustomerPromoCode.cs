﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

public class CustomerPromoCode
{
    public Customer Customer { get; set; }
    public Guid CustomerId { get; set; }

    public PromoCode PromoCode { get; set; }
    public Guid PromoCodeId { get; set; }
}