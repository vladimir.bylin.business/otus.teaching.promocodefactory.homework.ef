﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IBaseRepository<TEntity> where TEntity: BaseEntity
    {
        IQueryable<TEntity> GetAll(); 
        
        IQueryable<TEntity> GetByCondition(Expression<Func<TEntity, bool>> expression);
        
        void Create(TEntity entity);
        
        void Update(TEntity entity);
        
        void Delete(TEntity entity);

        Task SaveChangesAsync();
    }
}