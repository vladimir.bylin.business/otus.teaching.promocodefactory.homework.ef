﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface ICustomerRepository : IBaseRepository<Customer>
{
    Task<List<Customer>> GetAllAsync();

    Task<Customer> GetByIdAsync(Guid id);
    
    Task<Customer> GetByIdForUpdateAsync(Guid id);
    
    Task<List<Customer>> GetAllWithGivenPreference(Guid preferenceId);
    
    Task<Guid> CreateAsync(Customer customer);

    Task UpdateAsync(Customer customer);

    Task DeleteAsync(Guid id);
}