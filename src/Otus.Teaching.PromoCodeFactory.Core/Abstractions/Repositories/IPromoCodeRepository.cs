﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface IPromoCodeRepository : IBaseRepository<PromoCode>
{
    Task<List<PromoCode>> GetAllAsync();

    Task<PromoCode> GetByIdAsync(Guid id);

    Task<Guid> CreateAsync(PromoCode promoCode);

    Task UpdateAsync(PromoCode promoCode);
    
    Task UpdateRangeAsync(IEnumerable<PromoCode> promoCodes);

    Task DeleteAsync(Guid id);
}