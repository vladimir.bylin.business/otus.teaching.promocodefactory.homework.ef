﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

public interface IPreferenceRepository : IBaseRepository<Preference>
{
    Task<List<Preference>> GetAllAsync();

    Task<Preference> GetByIdAsync(Guid id);

    Task<Guid> CreateAsync(Preference preference);

    Task UpdateAsync(Preference preference);

    Task DeleteAsync(Guid id);
}