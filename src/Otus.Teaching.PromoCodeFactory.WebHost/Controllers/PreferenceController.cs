﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Предпочтения
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class PreferenceController : ControllerBase
{
    private readonly IPreferenceRepository _preferenceRepository;

    public PreferenceController(IPreferenceRepository preferenceRepository)
    {
        _preferenceRepository = preferenceRepository;
    }

    /// <summary>
    /// Получить список существующих предпочтений
    /// </summary>
    /// <returns>Список предпочтений</returns>
    [HttpGet]
    public async Task<ActionResult<List<PreferenceResponse>>> GetAllPreferences()
    {
        var preferences = await _preferenceRepository.GetAllAsync();

        return preferences.Select(p => new PreferenceResponse()
        {
            Id = p.Id,
            Name = p.Name
        }).ToList();
    }
    
    /// <summary>
    /// Создать новое предпочтение
    /// </summary>
    /// <param name="request">Модель запроса на создание предпочтения</param>
    /// <returns>Идентификатор созданного предпочтения</returns>
    [HttpPost]
    public async Task<ActionResult<Guid>> CreatePreference(CreatePreferenceRequest request)
    {
        var preferenceToCreate = new Preference
        {
            Name = request.Name
        };

        var id = await _preferenceRepository.CreateAsync(preferenceToCreate);

        return id;
    }
}