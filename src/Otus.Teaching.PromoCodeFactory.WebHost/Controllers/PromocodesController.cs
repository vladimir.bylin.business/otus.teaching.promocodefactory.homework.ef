﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IPromoCodeRepository _promoCodeRepository;
        private readonly ICustomerRepository _customerRepository;

        public PromocodesController(IPromoCodeRepository promoCodeRepository, ICustomerRepository customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns>Список промокодов</returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            if (promoCodes is null)
            {
                return StatusCode(404, $"Промокоды не были найдены.");
            }

            return promoCodes.Select(p => new PromoCodeShortResponse()
            {
                Id = p.Id,
                Code = p.Code,
                BeginDate = p.BeginDate.ToShortDateString(),
                EndDate = p.EndDate.ToShortDateString(),
                PartnerName = p.PartnerName,
                ServiceInfo = p.ServiceInfo
            }).ToList();
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var promoCodeToCreate = new PromoCode
            {
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                BeginDate = request.BeginDate,
                EndDate = request.EndDate,
                PreferenceId = request.PreferenceId,
                PartnerManagerId = request.ManagerId
            };
            
            await _promoCodeRepository.CreateAsync(promoCodeToCreate);

            var customers = await _customerRepository.GetAllWithGivenPreference(request.PreferenceId);

            foreach (var customer in customers)
            {
                customer.PromoCodes = new List<PromoCode> { promoCodeToCreate };

                await _customerRepository.UpdateAsync(customer);
            }
            
            return Ok();
        }
    }
}