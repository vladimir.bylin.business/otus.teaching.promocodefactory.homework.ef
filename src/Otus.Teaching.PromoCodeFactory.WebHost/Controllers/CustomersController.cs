﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IPreferenceRepository _preferenceRepository;
        
        public CustomersController(ICustomerRepository customerRepository, IPreferenceRepository preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        
        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns>Массив клиентов</returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            return customers.Select(c => new CustomerShortResponse
            {
                Id = c.Id,
                FirstName = c.FirstName,
                LastName = c.LastName,
                Email = c.Email
            }).ToList();
        }
        
        /// <summary>
        /// Получить подробную информацию о клиенте по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns>Подробная информация о клиенте</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer is null)
            {
                return NotFound($"Клиент с идентификатором {id} не найден");
            }
            
            return new CustomerResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.Preferences == null 
                    ? new List<PreferenceResponse>()
                    : customer.Preferences.Select(p => new PreferenceResponse
                    {
                        Id = p.Id,
                        Name = p.Name
                    }).ToList(),
                PromoCodes = customer.PromoCodes == null 
                    ? new List<PromoCodeShortResponse>()
                    : customer.PromoCodes.Select(p => new PromoCodeShortResponse
                    {
                        Id = p.Id,
                        Code = p.Code,
                        BeginDate = p.BeginDate.ToShortDateString(),
                        EndDate = p.EndDate.ToShortDateString(),
                        PartnerName = p.PartnerName,
                        ServiceInfo = p.ServiceInfo
                    }).ToList()
            };
        }
        
        /// <summary>
        /// Создать клиента с предпочтениями
        /// </summary>
        /// <param name="request">Модель запроса на создание клиента с предпочтениями</param>
        /// <returns>Идентификатор созданного клиента</returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
            
            if (request.PreferenceIds.Count != 0)
            {
                customer.CustomerPreferences = new List<CustomerPreference>();
                
                foreach (var preferenceId in request.PreferenceIds)
                {
                    var preference = await _preferenceRepository.GetByIdAsync(preferenceId);

                    if (preference is null)
                    {
                        return StatusCode(500, $"Указанное в запросе предпочтение с идентификатором" +
                                               $" {preferenceId} не найдено.");
                    }
                    
                    customer.CustomerPreferences.Add(new CustomerPreference
                    {
                        CustomerId = customer.Id,
                        PreferenceId = preferenceId
                    });
                }
            }
            
            var id = await _customerRepository.CreateAsync(customer);

            return Ok(id);
        }
        
        /// <summary>
        /// Редактироваать данные клиента с его предпочтениями по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <param name="request">Модель запроса на редактирование клиента и его предпочтений</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdForUpdateAsync(id);

            if (customer is null)
            {
                return NotFound($"Клиент с идентификатором {id} не найден");
            }

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            if (request.PreferenceIds.Count != 0)
            {
                // Удаляем предпочтения, которые есть у клиента, но не указаны в запросе (предполагается, что
                // в запросе указан список предпочтений, который должен соответствовать 1:1 с тем, что
                // находится в БД)
                foreach (var preference in customer.Preferences)
                {
                    if (!request.PreferenceIds.Contains(preference.Id))
                    {
                        customer.Preferences.Remove(preference);
                    }
                }
                
                // Добавляем в список предпочтений клиента те предпочтения из запроса, которых у клиента нет
                foreach (var preferenceId in request.PreferenceIds)
                {
                    var preference = await _preferenceRepository.GetByIdAsync(preferenceId);

                    if (preference == null)
                    {
                        return StatusCode(500, $"Указанное в запросе предпочтение с идентификатором" +
                                               $" {preferenceId} не найдено.");
                    }

                    if (customer.Preferences.All(p => p.Id != preferenceId))
                    {
                        customer.Preferences.Add(preference);
                    }
                }
            }
            
            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }
        
        /// <summary>
        /// Удалить клиента и выданные ему промокоды
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer is null)
            {
                return NotFound($"Клиент с идентификатором {id} не найден");
            }
            
            await _customerRepository.DeleteAsync(id);
            
            return Ok();
        }
    }
}