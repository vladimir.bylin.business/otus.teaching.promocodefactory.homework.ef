﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        public string ServiceInfo { get; set; }

        public string PartnerName { get; set; }

        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }
        
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid ManagerId { get; set; }
    }
}